<?
/*
if($_REQUEST["update"]){
    $configfile = Config::get('prefix') . "/config/apg.cfg.php";
    if(is_writable($configfile)){
        array_pop($_POST);
        $fileH=fopen($configfile,"w");
        fwrite($fileH, "<?\n");
        foreach($_POST as $key=>$value){
            $wstring="\$$key=\"$value\";\n";
            fwrite($fileH, $wstring);
        }
        fwrite($fileH,"?>\n");
        fclose($fileH);
    }//end is_writable
}
*/
?>
<form name=adminform method=post>
<B>WAS Version: <?php echo Config::get('version') ?></b>
<input type=hidden name=version value=<?php echo Config::get('version') ?>>

<h2>Database Setup:</h2>
<table border=1>
    <tr>
        <td>Database Server:</td>
        <td><input type=text name=dbserver value=<?php echo Config::get('database_hostname') ?>></td>
    </tr>
    <tr>
        <td>Database User:</td>
        <td><input type=text name=dbuser value=<?php echo Config::get('database_username') ?>></td>
    </tr>
    <tr>
        <td>Database Password:</td>
        <td><input type=password name=dbpwd value=<?php echo Config::get('database_password') ?>></td>
    </tr>
    <tr>
        <td>Database Name:</td>
        <td><input type=text name=dbname value=<?php echo Config::get('database_name') ?>></td>
    </tr>
</table>

<!-- Messages Per Page:<input type=text name=limit value=<?php echo Config::get('limit') ?>><br/> -->
<h2>Live Page Setup:</h2>
Meta Refresh for <b>Live</b> page
<input type="text" name="refreshRate" size="3" maxlength="3" value="<?php echo Config::get('refreshRate') ?>"> Seconds

<h2>Charts Page Setup:</h2>
<B>Select the graphs you would like to see on the stats page:</B>

<table border=1>
    <tr><td>
        <input type=checkbox name=usageChart <?php echo (Config::get('usageChart')) ? "checked" : ""; ?> >Usage Chart<br/>
        <input type=checkbox name=mostTalkChart <?php echo (Config::get('mostTalkChart')) ? "checked" : "";?>>Most Talkative Chart<br/>
        <input type=checkbox name=mostPopChart <?php echo (Config::get('mostPopChart')) ? "checked" : "";?>>Most Popular Chart<br/>
    </td></tr>
    <tr><td>
        <input type=checkbox name=outPerH <?php echo (Config::get('outPerH')) ? "checked" : "";?>>Outgoing Msgs Per Hour of Day<br/>
        <input type=checkbox name=inPerH <?php echo (Config::get('inPerH')) ? "checked" : "";?>>Incoming Msgs Per Hour of Day<br/>
        <input type=checkbox name=inoutPerH <?php echo (Config::get('inoutPerH')) ? "checked" : "";?>>Incoming & Outgoing Msgs Per Hour of Day (on same graph)<br/>
    </td></tr>
    <tr><td>
        <input type=checkbox name=outPerDOW <?php echo (Config::get('outPerDOW')) ? "checked" : "";?>>Outgoing Msgs Per Day of Week<br/>
        <input type=checkbox name=inPerDOW <?php echo (Config::get('inPerDOW')) ? "checked" : "";?>>Incoming Msgs Per Day of Week<br/>
        <input type=checkbox name=inoutPerDOW <?php echo (Config::get('inoutPerDOW')) ? "checked" : "";?>>Incoming & Outgoing Msgs Per Day of Week(on same graph)<br/>
    </td></tr>
    <tr><td>
        <input type=checkbox name=outPerDOM <?php echo (Config::get('outPerDOM')) ? "checked" : "";?>>Outgoing Msgs Per Day of Month<br/>
        <input type=checkbox name=inPerDOM <?php echo (Config::get('inPerDOM')) ? "checked" : "";?>>Incoming Msgs Per Day of Month<br/>
        <input type=checkbox name=inoutPerDOM <?php echo (Config::get('inoutPerDOM')) ? "checked" : "";?>>Incoming & Outgoing Msgs Per Day of Month(on same graph)<br/>
    </td></tr>
</table>

<input type=submit name=update value=Submit><BR>
</form>