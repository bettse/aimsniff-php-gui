<script type="text/javascript" language=javascript>
<!-- // Required to be compliant with XHTML-->
window.onload=start_reloading  //start the rounds
function start_reloading(){
    self.int_id = self.setInterval("get_next()", 1000);
}
function get_next(){
    var xmlHttp=null; // Defines that xmlHttp is a new variable.
    // Try to get the right object for different browser
    try {
        // Firefox, Opera 8.0+, Safari, IE7+
        xmlHttp = new XMLHttpRequest(); // xmlHttp is now a XMLHttpRequest.
    } catch (e) {
        // Internet Explorer
        try {
        xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4)
        try { // In some instances, status cannot be retrieved and will produce
                // an error (e.g. Port is not responsive)
            if (xmlHttp.status == 200) {
                // Set the main HTML of the body to the info provided by the
                // AJAX Request
                document.getElementById("ajax_output").innerHTML = xmlHttp.responseText;
            }
        } catch (e) {
            document.getElementById("ajax_output").innerHTML
                = "Error on Ajax return call : " + e.description;
        }
    }
    xmlHttp.open("get","singlemsg.php"); // .open(RequestType, Source);
    xmlHttp.send(null); // Since there is no supplied form, null takes its place
                        // as a new form.
}//end get_next
</script>
<div id="ajax_output">Waiting to be replaced by Ajax Call</div>
<!-- <button onclick="window.clearInterval(self_int)">Stop reloading</button> -->
