<div id="stats" align="center">

<?php

if(Config::get('usageChart')){
	//consider replacing this with an include of the main page since it has this graph and only this graph
    open_flash_chart_object(400, 250, Config::get('web_path') . '/ofc_data/typesGraph.php', false, Config::get('web_path') . '/lib/ofc/' );
    echo "<br/><br/>";
}
if(Config::get('mostTalkChart')){
    open_flash_chart_object(800, 250, Config::get('web_path') . '/ofc_data/mostTalk.php', false, Config::get('web_path') . '/lib/ofc/' );
    echo "<br><br/>";
}
if(Config::get('mostPopChart')){
    open_flash_chart_object(800, 250, Config::get('web_path') . '/ofc_data/mostPop.php', false, Config::get('web_path') . '/lib/ofc/' );
    echo "<br><br/>";
}
if(Config::get('outPerH')){
    open_flash_chart_object(1000, 250, Config::get('web_path') . '/ofc_data/outgoingMsgsPerHour.php', false, Config::get('web_path') . '/lib/ofc/' );
    echo "<br><br/>";
}
if(Config::get('inPerH')){
    open_flash_chart_object(1000, 250, Config::get('web_path') . '/ofc_data/incomingMsgsPerHour.php', false, Config::get('web_path') . '/lib/ofc/' );
    echo "<br><br/>";
}
if(Config::get('inoutPerH')){
    open_flash_chart_object(1000, 250, Config::get('web_path') . '/ofc_data/MsgsPerHour.php', false, Config::get('web_path') . '/lib/ofc/' );
    echo "<br><br/>";
}
if(Config::get('outPerDOW')){
    open_flash_chart_object(700, 250, Config::get('web_path') . '/ofc_data/outgoingMsgsPerDOW.php', false, Config::get('web_path') . '/lib/ofc/' );
    echo "<br><br/>";
}
if(Config::get('inPerDOW')){
    open_flash_chart_object(700, 250, Config::get('web_path') . '/ofc_data/incomingMsgsPerDOW.php', false, Config::get('web_path') . '/lib/ofc/' );
    echo "<br><br/>";
}
if(Config::get('inoutPerDOW')){
    open_flash_chart_object(700, 250, Config::get('web_path') . '/ofc_data/MsgsPerDOW.php', false, Config::get('web_path') . '/lib/ofc/' );
    echo "<br><br/>";
}
if(Config::get('outPerDOM')){
    open_flash_chart_object(1000, 250, Config::get('web_path') . '/ofc_data/outgoingMsgsPerDOM.php', false, Config::get('web_path') . '/lib/ofc/' );
    echo "<br><br/>";
}
if(Config::get('inPerDOM')){
    open_flash_chart_object(1000, 250, Config::get('web_path') . '/ofc_data/incomingMsgsPerDOM.php', false, Config::get('web_path') . '/lib/ofc/' );
    echo "<br><br/>";
}
if(Config::get('inoutPerDOM')){
    open_flash_chart_object(1000, 250, Config::get('web_path') . '/ofc_data/MsgsPerDOM.php', false, Config::get('web_path') . '/lib/ofc/' );
}

?>
</div>
