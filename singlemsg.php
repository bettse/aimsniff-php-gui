<?php
require_once("./lib/init.php");
$sql="SELECT UNIX_TIMESTAMP(ts), ip, fromHandle, handle, message FROM logs ORDER BY ts DESC LIMIT 1";
$result=Dba::query($sql);
$rs=Dba::fetch_row($result);
$time_since_message = time() - $rs[0];
?>

<div class="im" align="center">
<?echo  $time_since_message ?> seconds ago<br/>
<?echo $rs[2] ?>  ->  <?echo $rs[3] ?><br/>
<?echo strip_tags($rs[4]) ?>
</div>

