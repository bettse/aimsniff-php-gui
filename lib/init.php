<?php
/*
 * This is the init file which glues stuff together
 * Yep it looks like ampache
 * GPL header goes here yada yada....
 */

// Use output buffering, this gains us a few things and
// fixes some CSS issues
ob_start();

error_reporting(E_ALL ^ E_NOTICE);

$was2_path 	= dirname(__FILE__);
$prefix		= realpath($was2_path . "/../");

$configfile = "$prefix/config/apg.cfg.php";
require_once $prefix . '/lib/general.lib.php';
require_once $prefix . '/lib/class/config.class.php';
require_once $prefix . '/lib/ofc/open_flash_chart_object.php';

// Define some base level config options
Config::set('prefix',$prefix);

/*
 Check to see if this is Http or https
 */
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
	$http_type = "https://";
}
else {
	$http_type = "http://";
}

/*
 See if the Config File Exists
*/
if (!file_exists($configfile)) {
        echo "Missing config file";
        exit();
}

// Use the built in PHP function, supress errors here so we can handle it properly
$results = @parse_ini_file($configfile);

if (!count($results)) {
        echo "Empty config file";
        exit();
}



$result['version']			= "2.0";

$results['web_path'] =  preg_replace("/(.*)\/(\w+\.php)$/","\${1}", $_SERVER['PHP_SELF']);

$results['raw_web_path']	= $results['web_path'];
$results['web_path']		= $http_type . $_SERVER['HTTP_HOST'] . $results['web_path'];
$results['http_port']           = $_SERVER['SERVER_PORT'];
if (!$results['http_port']) {
        $results['http_port']   = '80';
}
if (!$results['site_charset']) {
        $results['site_charset'] = "UTF-8";
}
if (!$results['raw_web_path']) {
        $results['raw_web_path'] = '/';
}
if (!$_SERVER['SERVER_NAME']) {
        $_SERVER['SERVER_NAME'] = '';
}
if (!$results['user_ip_cardinality']) {
        $results['user_ip_cardinality'] = 42;
}

define('INIT_LOADED','1');



// $results['prefix'] = $prefix; //not in the original ampache file, not sure where it came from

Config::set_by_array($results,1);
?>
