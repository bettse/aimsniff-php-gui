<?php
/*

 Copyright (c) 2001 - 2007 Ampache.org
 All rights reserved.

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License v2
 as published by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/


/**
 * ip2int
 * turns a dotted quad ip into an int
 */
function ip2int($ip) {

    $a=explode(".",$ip);
	return $a[0]*256*256*256+$a[1]*256*256+$a[2]*256+$a[3];

} // ip2int

/*!
	@function int2ip
	@discussion turns a int into a dotted quad
*/
function int2ip($i) {
        $d[0]=(int)($i/256/256/256);
        $d[1]=(int)(($i-$d[0]*256*256*256)/256/256);
        $d[2]=(int)(($i-$d[0]*256*256*256-$d[1]*256*256)/256);
        $d[3]=$i-$d[0]*256*256*256-$d[1]*256*256-$d[2]*256;
	return "$d[0].$d[1].$d[2].$d[3]";
} // int2ip

/*
 * Conf function by Robert Hopson
 * call it with a $parm name to retrieve
 * a var, pass it a array to set them
 * to reset a var pass the array plus
 * Clobber! replaces global $conf;
*/
/*function conf($param,$clobber=0)
{
        static $params = array();

        if(is_array($param))
        //meaning we are setting values
        {
                foreach ($param as $key=>$val)
                {
                        if(!$clobber && isset($params[$key]))
                        {
                                echo "Error: attempting to clobber $key = $val\n";
                                exit();
                        }
                        $params[$key] = $val;
                }
                return true;
        }
        else
        //meaning we are trying to retrieve a parameter
        {
                if($params[$param]) return $params[$param];
                else return;
        }
} //conf

function error_results($param,$clobber=0)
{
        static $params = array();

        if(is_array($param))
        //meaning we are setting values
        {
                foreach ($param as $key=>$val)
                {
                        if(!$clobber && isset($params[$key]))
                        {
                                echo "Error: attempting to clobber $key = $val\n";
                                exit();
                        }
                        $params[$key] = $val;
                }
                return true;
        }
        else
        //meaning we are trying to retrieve a parameter
        {
                if($params[$param]) return $params[$param];
                else return;
        }
} //error_results
*/

/*!
	@function scrub_in()
	@discussion Run on inputs, stuff that might get stuck in our db
*/
function scrub_in($str) {

        if (!is_array($str)) {
                return stripslashes( htmlspecialchars( strip_tags($str) ) );
        }
        else {
                $ret = array();
                foreach($str as $string) $ret[] = scrub_in($string);
                return $ret;
        }
} // scrub_in

/**
 * generate_password
 * This generates a random password, of the specified
 * length
 */
function generate_password($length) {

    $vowels = 'aAeEuUyY12345';
    $consonants = 'bBdDgGhHjJmMnNpPqQrRsStTvVwWxXzZ6789';
    $password = '';

    $alt = time() % 2;

    for ($i = 0; $i < $length; $i++) {
        if ($alt == 1) {
            $password .= $consonants[(rand() % 39)];
            $alt = 0;
        } else {
            $password .= $vowels[(rand() % 17)];
            $alt = 1;
        }
    }

    return $password;

} // generate_password

/**
 * scrub_out
 * This function is used to escape user data that is getting redisplayed
 * onto the page, it htmlentities the mojo
 */
function scrub_out($str) {

	if (get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}

        $str = htmlentities($str,ENT_QUOTES,Config::get('site_charset'));

        return $str;

} // scrub_out

/**
 * show_time_select
 * This function shows a 24hr drop down amazing!
 */
function show_time_select($selected,$name,$divisor=15) {


    $start = round(time()/86400)*86400 + (3600*4);

    $end   = round(time()/86400)*86400 + (3600*11);

    $add = $divisor*60;

    echo "<select id=\"$name\" name=\"$name\">\n";
    echo "\t<option value=\"\">- Select Time -</option>";

    while ($start <= $end) {
        $select_txt = '';
        if ($start == $selected) { $select_txt = "selected=\"selected\""; }
        echo "\t<option value=\"$start\" $select_txt>" . date("h:i A",$start) . "</option>\n";
        $start = $start + $add;
    } // end for loop

    echo "</select>\n";

} // show_time_select

/**
 * make_error
 * This just formats a nice little error message
 * for them
 */
function make_error($string) {

    return "<span class=\"error\">" . scrub_out($string) . "</span>";

} // make_error

/**
 * make_confrim
 * This just forms a nice little confirmation message
 */
function make_confirm($string) {

    return "<span class=\"confirm\">" . scrub_out($string) . "</span>";

} // make_confrim

/**
 * revert_string
 * This returns a scrubed string to it's most normal state
 * Uhh yea better way to do this please?
 */
function revert_string($string) {

	$string = unhtmlentities($string,ENT_QUOTES,Config::set('site_charset'));
	return $string;

} // revert_string

/**
 * make_bool
 * This takes a value and returns what I consider to be the correct boolean value
 * This is used instead of settype alone because settype considers 0 and "false" to
 * be true
 * @package General
 */
function make_bool($string) {

	if (strcasecmp($string,'false') == 0) {
		return '0';
	}

	if ($string == '0') {
		return '0';
	}

	if (strlen($string) < 1) {
		return '0';
	}

	return settype($string,"bool");

} // make_bool

/**
 * format_time
 * This formats seconds into minutes:seconds
 */

function format_time($seconds) {

return sprintf ("%d:%02d", $seconds/60, $seconds % 60);

} //format_time

/**
 * print_boolean
 * This function takes a boolean value and then print out  a friendly
 * text message, usefull if you have a 0/1 that you need to turn into
 * a "Off" "On"
 */
function print_boolean($value) {


	if ($value) {
		$string = '<span class="item_on">' . _('On') . '</span>';
	}
	else {
		$string = '<span class="item_off">' . _('Off') . '</span>';
	}

	return $string;

} // print_boolean

/**
 * invert_boolean
 * This returns the opposite of what you've got
 */
function invert_boolean($value) {

	if (make_bool($value)) {
		return '0';
	}
	else {
		return '1';
	}

} // invert_boolean

/**
 * unhtmlentities
 * This is required to make thing work.. but holycrap is it ugly
 */
function unhtmlentities ($string)  {

        $trans_tbl = get_html_translation_table (HTML_ENTITIES);
        $trans_tbl = array_flip ($trans_tbl);
        $ret = strtr ($string, $trans_tbl);
        return preg_replace('/&#(\d+);/me', "chr('\\1')",$ret);

} // unhtmlentities

/**
 * __autoload
 * This function automatically loads any missing
 * classes as they are called so that we don't have to have
 * a million include statements, and load more then we need
 */
function __autoload($class) {
	// Lowercase the class
    $class = strtolower($class);

	$file = Config::get('prefix') . "/lib/class/$class.class.php";

	// See if it exists
        if (is_readable($file)) {
                require_once $file;
                if (is_callable($class . '::_auto_init')) {
                        call_user_func(array($class, '_auto_init'));
                }
        }
	// Else log this as a fatal error
        else {
                debug_event('__autoload', "'$class' not found!",'1');
        }

} // __autoload

?>
