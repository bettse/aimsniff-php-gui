<?
require_once("../lib/init.php");
include_once( '../lib/ofc/open-flash-chart.php' );

$title = "Most Popular Users";

//collect the data
$sql="SELECT Handle, count(message) as msgCnt FROM logs WHERE direction='00040007' and handle<>'NULL' GROUP BY handle ORDER BY msgCnt DESC LIMIT 10";
$result=Dba::query($sql);
$data = array();

$label=array();
$value=array();
$i=0;

while($rs=Dba::fetch_row($result)){
  $temp=array($rs[0], $rs[1]);
  $temp2=$data;
  $data=array_merge($temp,$temp2);


  if($rs[0]!= NULL){
    $label[$i]=$rs[0];
  }else{
    $label[$i]="";
  }

  if($rs[1]!= NULL){
    $value[$i++]=$rs[1];
  }else{
    $value[$i++]=0;
  }

}

//make the graph
include_once( 'graph_defaults.php' );

// $bar = new bar_outline( 50, $incolor, $outlinecolor );
$bar = new bar_glass( 50, $incolor, $outlinecolor );
$bar->key( 'Messages in', 10 );
$bar->data = $value;

$g->data_sets[] = $bar;
if(isset($bar2)){
    $g->data_sets[] = $bar2;
}

$g->set_x_label_style( 10, $labelcolor, 0);

echo $g->render();

?>

