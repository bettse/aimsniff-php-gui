<?
require_once("../lib/init.php");
include_once( '../lib/ofc/open-flash-chart.php' );

$title = "Per Day of Month";

//collect the data
$label=array();

//collect out data
$sql="SELECT DATE_FORMAT(ts,\"%d\") as dayoM, count(message) as msgCnt FROM logs WHERE direction='00040006' GROUP BY dayoM ORDER BY dayoM ASC";
$result=Dba::query($sql);
$value=array();
$i=0;

while($rs=Dba::fetch_row($result)){
  if($rs[0]!= NULL){
    $label[$i]=$rs[0];
  }else{
    $label[$i]="";
  }
  if($rs[1]!= NULL){
    $value[$i++]=$rs[1];
  }else{
    $value[$i++]=0;
  }
}

//collect in data
$sql2="SELECT DATE_FORMAT(ts,\"%d\") as dayoM, count(message) as msgCnt FROM logs WHERE direction='00040007' GROUP BY dayoM ORDER BY dayoM ASC";
$result2=Dba::query($sql2);
$value2=array();
$i=0;

while($rs=Dba::fetch_row($result2)){
  if($rs[0]!= NULL){
    $label[$i]=$rs[0];
  }else{
    $label[$i]="";
  }
  if($rs[1]!= NULL){
    $value2[$i++]=$rs[1];
  }else{
    $value2[$i++]=0;
  }
}

//make the graph
include_once( 'graph_defaults.php' );

//$bar = new bar_outline( 50, $outcolor, $outlinecolor );
$bar = new bar_3d( 75, $outcolor );
$bar->key( 'Messages out', 10 );
$bar->data = $value;

// $bar2 = new bar_outline( 50, $incolor, $outlinecolor );
$bar2 = new bar_3d( 75, $incolor );
$bar2->key( 'Messages in', 10 );
$bar2->data = $value2;

$g->data_sets[] = $bar;
if(isset($bar2)){
    $g->data_sets[] = $bar2;
}

$g->set_x_label_style( 10, $labelcolor, 0, 2);
$g->set_x_axis_3d( 12 );
$g->x_axis_colour( '#909090', '#ADB5C7' );
$g->y_axis_colour( '#909090', '#ADB5C7' );
$g->set_x_axis_steps( 2 );

echo $g->render();
?>