<?php
require_once("../lib/init.php");
include_once( '../lib/ofc/open-flash-chart.php' );

//collect the data from the database
$sql="SELECT count(message) FROM logs";
$result=Dba::query($sql);
$rs=Dba::fetch_row($result);
$total=$rs[0];

$sql="SELECT count(id) from handles";
$result=Dba::query($sql);
$rs=Dba::fetch_row($result);
$loginsCnt=$rs[0];
$total=$total+$rs[0];

$sql="SELECT count(id) from buddies";
$result=Dba::query($sql);
$rs=Dba::fetch_row($result);
$buddyCnt=$rs[0];
$total=$total+$rs[0];

$sql="SELECT count(message) FROM logs WHERE message like 'File%'";
$result=Dba::query($sql);
if(Dba::num_rows($result)){
  $rs=Dba::fetch_row($result);
  $fileCnt=$rs[0];
}else{
  $fileCnt=0;
}

$sql="SELECT count(message) FROM logs WHERE message like '***CHAT%'";
$result=Dba::query($sql);
if(Dba::num_rows($result)){
  $rs=Dba::fetch_row($result);
  $chatCnt=$rs[0];
}else{
  $chatCnt=0;
}

$sql="SELECT count(message) FROM logs WHERE (message NOT LIKE 'File%' AND message NOT LIKE '***CHAT%')";
$result=Dba::query($sql);
if(Dba::num_rows($result)){
  $rs=Dba::fetch_row($result);
  $msgCnt=$rs[0];
}else{
  $msgCnt=0;
}


$loginsCnt = round((($loginsCnt/$total)*100), 2);
$buddyCnt = round((($buddyCnt/$total)*100), 2);
$fileCnt = round((($fileCnt/$total)*100), 2);
$chatCnt = round((($chatCnt/$total)*100), 2);
$msgCnt = round((($msgCnt/$total)*100), 2);


$g = new graph();

//
// PIE chart, 60% alpha
//
$g->pie(60,'#505050','{font-size: 12px; color: #404040;');
$g->bg_colour = '#FFFFFF';

//
// pass in two arrays, one of data, the other data labels
//
$g->pie_values(
    array (
        $loginsCnt,
        $buddyCnt,
        $fileCnt,
        $chatCnt,
        $msgCnt),
    array(
        'Logins',
        'Buddy Lists',
        'File Xfers',
        'Chats',
        'Messages')
);//add a 3rd array with URLs to link pie chart pieces

//
// Colours for each slice, in this case some of the colours
// will be re-used (3 colurs for 5 slices means the last two
// slices will have colours colour[0] and colour[1]):
//
$g->pie_slice_colours( array('#d01f3c','#356aa0','#C79810') ); //can't seem to figure out how to get it to use 5 colors, but then again it might be the disparity between percentages commonly seen

$g->set_tool_tip( 'Type: #x_label#<br>Percentage: #val#%' );

$g->title( 'Packet types', '{font-size:18px; color: #d01f3c}' );
echo $g->render();

?>