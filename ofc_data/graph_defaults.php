<?
$incolor = '#DD3388';
$outcolor = '#8833DD';
$labelcolor = '#8010A0';
$outlinecolor = '#8010A0';

$g = new graph();
// $g->title( $title, '{font-size: 20px;}' );
$g->title( $title, '{font-size:20px; color: #FFFFFF; margin: 5px; background-color: #505050; padding:5px; padding-left: 20px; padding-right: 20px;}' );

$g->bg_colour = '#FFFFFF';

$g->set_x_labels( $label );

if(isset($value2)){
    $g->set_y_max( max(array_merge($value, $value2)) );
}else{
    $g->set_y_max( max($value) );
}
$g->y_label_steps( 4 );
$g->set_tool_tip( '#x_label#<br>Count: #val#' );

?>