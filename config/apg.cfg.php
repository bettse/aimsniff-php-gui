;#<?php exit(); ?>##
;###################
; General Config   #
;###################

; This value is used to detect quickly
; if this config file is up to date
; this is compared against a value hardcoded
; into the init script
version = 2.0

; Hostname of your Database
; DEFAULT: localhost
database_hostname = localhost

; Name of your aimsniff database
; DEFAULT: aimsniff
database_name = aimsniff

; Username for your aimsniff database
; DEFAULT: ""
database_username = aimsniff

; Password for your ampache database, this can not be blank
; this is a 'forced' security precaution, the default value
; will not work
; DEFAULT: ""
database_password = aimsniff


limit = 20
refreshRate = 60


usageChart = true
mostTalkChart = true
mostPopChart = true

outPerH = false
inPerH = false
inoutPerH = true

outPerDOW = false
inPerDOW = false
inoutPerDOW = true

outPerDOM = false
inPerDOM = false
inoutPerDOM = true
