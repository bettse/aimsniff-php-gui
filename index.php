<?php
/*
 * Yeah magic
 */
require_once("./lib/init.php");

require ("global.php");//get rid of this soon

require(Config::get('prefix') . "/templates/header.inc.php");
require(Config::get('prefix') . "/templates/menu.inc.php");

?><div id="content"><?php

switch ($_REQUEST['action']) {
        case 'home':
                require(Config::get('prefix') . "/templates/main.inc.php");
        break;
        case 'charts':
                require(Config::get('prefix') . "/templates/charts.inc.php");
        break;
        case 'live':
                require(Config::get('prefix') . "/templates/live.inc.php");
        break;
        case 'filter':
                require(Config::get('prefix') . "/templates/filter.inc.php");
        break;
        case 'admin':
                require(Config::get('prefix') . "/templates/admin.inc.php");
        break;
        case 'search':
                require(Config::get('prefix') . "/templates/search.inc.php");
        break;
        case 'ajax':
                require(Config::get('prefix') . "/templates/ajax.inc.php");
        break;
        default:
                require(Config::get('prefix') . "/templates/main.inc.php");
        break;

}

?></div><?php

require(Config::get('prefix') . "/templates/footer.inc.php");
?>
